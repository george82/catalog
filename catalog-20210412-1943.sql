-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 12, 2021 at 04:43 PM
-- Server version: 5.7.21
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `catalog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `featured_image` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `title`, `slug`, `featured_image`, `description`, `is_active`, `lft`, `rght`, `created`, `modified`) VALUES
(1, NULL, 'Materiale de construcții', 'materiale-de-constructii', '/uploads/2021/04/4eccc07fecdd7d03-original.jpg', 'Lorem ipsum materiale de construcții', 1, 1, 2, '2021-04-02 16:05:20', '2021-04-09 22:11:08'),
(2, NULL, 'Piese Auto', 'piese-auto', '', 'Lorem ipsum piese auto', 1, 3, 6, '2021-04-02 16:18:35', '2021-04-02 16:18:35'),
(3, NULL, 'test', 'test', '', 'tessst', 1, 7, 8, '2021-04-08 08:01:19', '2021-04-08 08:01:19'),
(6, 2, 'cutie viteze', 'cutie-viteze', NULL, '<p>Lorem ipsum cutie viteze<br></p>', 1, 4, 5, '2021-04-11 14:51:55', '2021-04-11 14:51:55');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` text NOT NULL,
  `message` text NOT NULL,
  `created` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `phone`, `email`, `message`, `created`) VALUES
(1, 'test contact', '0769171712', 'george@behappydigital.com', '<p>test mesaj contact<br></p>', '2021-04-10 09:12:35'),
(2, 'aaaaaa', '01234544888', 'stanescu.gheorghe@yahoo.com', 'tesst front ', '2021-04-10 09:27:33');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

DROP TABLE IF EXISTS `gallery_images`;
CREATE TABLE IF NOT EXISTS `gallery_images` (
  `id` int(11) NOT NULL,
  `foreign_key` int(11) NOT NULL,
  `model` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `video_code` varchar(20) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `featured_image` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `uuid`, `parent_id`, `category_id`, `title`, `description`, `slug`, `featured_image`, `keywords`, `is_active`, `lft`, `rght`, `created`, `modified`) VALUES
(1, '', NULL, 1, 'Cărămidă', 'Lorem ipsum Cărămidă', 'caramida', '/uploads/2021/04/56c46e3487897158-original.png', '', 1, 1, 4, '2021-04-02 17:40:41', '2021-04-09 22:02:28'),
(2, '', 1, 2, 'test', 'tessst', 'test', '/uploads/2021/04/309b93a5119c407c-original.jpg', '', 1, 2, 3, '2021-04-08 08:06:37', '2021-04-09 22:00:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuid`, `full_name`, `email`, `password`, `is_active`, `created`, `modified`) VALUES
(8, '', 'George', 'stanescu.gheorghe@yahoo.com', '$2y$10$.0TxVNxzYPTqrQDn8ph7y.sRmYgDwCGXC2qmCzjxdgt..b.xAjVp.', 1, '2021-04-08 08:42:07', '2021-04-08 21:15:15'),
(9, '', 'George', 'gheorghe.stanescu@gmail.com', '$2y$10$g15ndbID.MxIXdlA8CvJ.esbXmLYE1.kQjVksP8gkUgm1z9Soob9a', 1, '2021-04-09 11:45:09', '2021-04-09 11:45:09'),
(10, '', 'test', 'test@test.test', '$2y$10$0dzDmQwAtQLAYEc27AB4IepqWcao.7auDHpPmsG/FrulZIFLoXGza', 1, '2021-04-09 17:37:00', '2021-04-09 17:37:00'),
(11, '6070cc237ec40', 'aaaaaaass', 'aaa@aaa.aaa', '$2y$10$aJU1xSBojuTW20xx2HDV8e5qai9CbKjZw6j.WQUyYnXbLLw2g8DTK', 1, '2021-04-09 21:50:27', '2021-04-09 21:50:50');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
