-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 10, 2021 at 12:20 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `catalog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `featured_image` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `title`, `slug`, `featured_image`, `description`, `is_active`, `lft`, `rght`, `created`, `modified`) VALUES
(1, NULL, 'Materiale de construcții', 'materiale-de-constructii', '/uploads/2021/04/4eccc07fecdd7d03-original.jpg', 'Lorem ipsum materiale de construcții', 1, 1, 4, '2021-04-02 16:05:20', '2021-04-09 22:11:08'),
(2, NULL, 'Piese Auto', 'piese-auto', '/uploads/2021/04/be39c1a98754459e-original.jpg', 'Lorem ipsum piese auto', 1, 5, 10, '2021-04-02 16:18:35', '2021-04-19 12:57:36'),
(3, NULL, 'test', 'test', '', 'tessst', 1, 11, 12, '2021-04-08 08:01:19', '2021-04-08 08:01:19'),
(6, 2, 'cutie viteze', 'cutie-viteze', NULL, '<p>Lorem ipsum cutie viteze<br></p>', 1, 6, 7, '2021-04-11 14:51:55', '2021-04-11 14:51:55'),
(7, 1, 'tesssst cat', 'tesssst-cat', '/uploads/2021/04/bba8172496d94f4c-original.jpg', '<p>tesssst cat \r\nDescription\r\n\r\n<br></p>', 1, 2, 3, '2021-04-16 17:25:36', '2021-04-16 17:25:36'),
(8, 2, 'Motoare', 'motoare', '/uploads/2021/04/5b55940dcd9c35c4-original.jpg', '<p>Lorem ipsum motor est<br></p>', 1, 8, 9, '2021-04-19 18:45:39', '2021-04-19 19:04:57');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` text NOT NULL,
  `message` text NOT NULL,
  `created` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `phone`, `email`, `message`, `created`) VALUES
(1, 'test contact', '0769171712', 'george@behappydigital.com', '<p>test mesaj contact<br></p>', '2021-04-10 09:12:35'),
(2, 'aaaaaa', '01234544888', 'stanescu.gheorghe@yahoo.com', 'tesst front ', '2021-04-10 09:27:33');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

DROP TABLE IF EXISTS `gallery_images`;
CREATE TABLE IF NOT EXISTS `gallery_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `foreign_key` int(11) NOT NULL,
  `model` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `video_code` varchar(20) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery_images`
--

INSERT INTO `gallery_images` (`id`, `foreign_key`, `model`, `title`, `image_path`, `video_code`, `created`, `modified`) VALUES
(25, 1, 'Products', '', '/uploads/2021/04/a4771d2e28053b0e-original.jpg', 'aEuFYHR-1TU', '2021-04-27 20:02:41', '2021-05-05 17:25:22'),
(26, 1, 'Products', NULL, '/uploads/2021/04/09c8d30b3fefe108-original.jpg', NULL, '2021-04-27 20:03:13', '2021-04-27 20:03:13'),
(27, 1, 'Products', NULL, '/uploads/2021/04/4034858f9834827d-original.jpg', NULL, '2021-04-27 20:04:01', '2021-04-27 20:04:01'),
(28, 1, 'Products', NULL, '/uploads/2021/04/8370ae45482f919c-original.jpg', NULL, '2021-04-27 20:04:30', '2021-04-27 20:04:30'),
(29, 1, 'Products', NULL, '/uploads/2021/04/1011c9bf7439f25b-original.jpg', NULL, '2021-04-27 20:07:27', '2021-04-27 20:07:27'),
(30, 1, 'Products', NULL, '/uploads/2021/04/f931c55fa7af9eb4-original.jpg', NULL, '2021-04-27 20:07:27', '2021-04-27 20:07:27'),
(31, 1, 'Products', NULL, '/uploads/2021/05/f79b175530be250b-original.jpg', NULL, '2021-05-05 18:36:02', '2021-05-05 18:36:02'),
(32, 1, 'Products', NULL, '/uploads/2021/05/d8d8148fca3b8dd0-original.jpg', NULL, '2021-05-05 18:36:03', '2021-05-05 18:36:03');

-- --------------------------------------------------------

--
-- Table structure for table `phinxlog`
--

DROP TABLE IF EXISTS `phinxlog`;
CREATE TABLE IF NOT EXISTS `phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phinxlog`
--

INSERT INTO `phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20210412164916, 'Initial', '2021-04-12 13:49:16', '2021-04-12 13:49:16', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` int(10) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `featured_image` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL,
  `rght` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `uuid`, `parent_id`, `category_id`, `title`, `description`, `price`, `slug`, `featured_image`, `keywords`, `is_active`, `lft`, `rght`, `created`, `modified`) VALUES
(1, '', NULL, 1, 'Cărămidă', 'Lorem ipsum Cărămidă', 2022, 'caramida', '/uploads/2021/04/56c46e3487897158-original.png', '', 1, 1, 4, '2021-04-02 17:40:41', '2021-04-16 17:24:02'),
(2, '', 1, 2, 'test', 'tessst', 994, 'test', '/uploads/2021/04/309b93a5119c407c-original.jpg', '', 1, 2, 3, '2021-04-08 08:06:37', '2021-04-16 19:14:25'),
(3, '', NULL, 1, 'test produs', '<p>test produs \r\nDescription\r\n\r\n<br></p>', 44, 'test-produs', '/uploads/2021/04/ffd359101c4a42a6-original.jpg', '', 1, 5, 6, '2021-04-16 17:28:55', '2021-04-16 17:28:55'),
(4, '', NULL, 8, 'Motor VW GOLF V 1.9 TDI', '<p>Descriere Motor VW GOLF V 1.9 TDI<br></p>', 1200, 'motor-vw-golf-v-1-9-tdi', '/uploads/2021/04/70f1a18c0e7cb96a-original.jpg', '', 1, 7, 8, '2021-04-19 18:54:08', '2021-04-19 18:54:08'),
(5, '', NULL, 8, 'Motor Hyundai i30 2014 - 1.6 CRDI', '<p>Descriere motor hyundai i30, 1.6 CRDI<br></p>', 3000, 'motor-hyundai-i30-2014-1-6-crdi', '/uploads/2021/04/676bd0b92bf54c10-original.jpg', '', 1, 9, 10, '2021-04-19 19:10:43', '2021-04-19 19:10:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuid`, `full_name`, `email`, `password`, `is_active`, `created`, `modified`) VALUES
(9, '', 'George', 'gheorghe.stanescu@gmail.com', '$2y$10$g15ndbID.MxIXdlA8CvJ.esbXmLYE1.kQjVksP8gkUgm1z9Soob9a', 1, '2021-04-09 11:45:09', '2021-04-09 11:45:09'),
(12, '607724e843cf2', 'admin', 'admin@catalog.lo', '$2y$10$OUWti8ux9oAwteB0n4SRp.zQxYulueZSJnVGcbWiMrIZRDeOpdrn2', 1, '2021-04-14 17:22:48', '2021-04-14 17:22:48');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
