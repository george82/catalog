<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Categories seed.
 */
class CategoriesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'parent_id' => NULL,
                'title' => 'Materiale de construcții',
                'slug' => 'materiale-de-constructii',
                'featured_image' => '/uploads/2021/04/4eccc07fecdd7d03-original.jpg',
                'description' => 'Lorem ipsum materiale de construcții',
                'is_active' => '1',
                'lft' => '1',
                'rght' => '2',
                'created' => '2021-04-02 16:05:20',
                'modified' => '2021-04-09 22:11:08',
            ],
            [
                'id' => '2',
                'parent_id' => NULL,
                'title' => 'Piese Auto',
                'slug' => 'piese-auto',
                'featured_image' => '',
                'description' => 'Lorem ipsum piese auto',
                'is_active' => '1',
                'lft' => '3',
                'rght' => '6',
                'created' => '2021-04-02 16:18:35',
                'modified' => '2021-04-02 16:18:35',
            ],
            [
                'id' => '3',
                'parent_id' => NULL,
                'title' => 'test',
                'slug' => 'test',
                'featured_image' => '',
                'description' => 'tessst',
                'is_active' => '1',
                'lft' => '7',
                'rght' => '8',
                'created' => '2021-04-08 08:01:19',
                'modified' => '2021-04-08 08:01:19',
            ],
            [
                'id' => '6',
                'parent_id' => '2',
                'title' => 'cutie viteze',
                'slug' => 'cutie-viteze',
                'featured_image' => NULL,
                'description' => '<p>Lorem ipsum cutie viteze<br></p>',
                'is_active' => '1',
                'lft' => '4',
                'rght' => '5',
                'created' => '2021-04-11 14:51:55',
                'modified' => '2021-04-11 14:51:55',
            ],
        ];

        $table = $this->table('categories');
        $table->insert($data)->save();
    }
}
