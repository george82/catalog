<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Contacts seed.
 */
class ContactsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'test contact',
                'phone' => '0769171712',
                'email' => 'george@behappydigital.com',
                'message' => '<p>test mesaj contact<br></p>',
                'created' => '2021-04-10 09:12:35',
            ],
            [
                'id' => '2',
                'name' => 'aaaaaa',
                'phone' => '01234544888',
                'email' => 'stanescu.gheorghe@yahoo.com',
                'message' => 'tesst front ',
                'created' => '2021-04-10 09:27:33',
            ],
        ];

        $table = $this->table('contacts');
        $table->insert($data)->save();
    }
}
