<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Products seed.
 */
class ProductsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'uuid' => '',
                'parent_id' => NULL,
                'category_id' => '1',
                'title' => 'Cărămidă',
                'description' => 'Lorem ipsum Cărămidă',
                'slug' => 'caramida',
                'featured_image' => '/uploads/2021/04/56c46e3487897158-original.png',
                'keywords' => '',
                'is_active' => '1',
                'lft' => '1',
                'rght' => '4',
                'created' => '2021-04-02 17:40:41',
                'modified' => '2021-04-09 22:02:28',
            ],
            [
                'id' => '2',
                'uuid' => '',
                'parent_id' => '1',
                'category_id' => '2',
                'title' => 'test',
                'description' => 'tessst',
                'slug' => 'test',
                'featured_image' => '/uploads/2021/04/309b93a5119c407c-original.jpg',
                'keywords' => '',
                'is_active' => '1',
                'lft' => '2',
                'rght' => '3',
                'created' => '2021-04-08 08:06:37',
                'modified' => '2021-04-09 22:00:32',
            ],
        ];

        $table = $this->table('products');
        $table->insert($data)->save();
    }
}
