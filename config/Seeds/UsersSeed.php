<?php
declare(strict_types=1);

use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '8',
                'uuid' => '',
                'full_name' => 'George',
                'email' => 'stanescu.gheorghe@yahoo.com',
                'password' => '$2y$10$.0TxVNxzYPTqrQDn8ph7y.sRmYgDwCGXC2qmCzjxdgt..b.xAjVp.',
                'is_active' => '1',
                'created' => '2021-04-08 08:42:07',
                'modified' => '2021-04-08 21:15:15',
            ],
            [
                'id' => '9',
                'uuid' => '',
                'full_name' => 'George',
                'email' => 'gheorghe.stanescu@gmail.com',
                'password' => '$2y$10$g15ndbID.MxIXdlA8CvJ.esbXmLYE1.kQjVksP8gkUgm1z9Soob9a',
                'is_active' => '1',
                'created' => '2021-04-09 11:45:09',
                'modified' => '2021-04-09 11:45:09',
            ],
            [
                'id' => '10',
                'uuid' => '',
                'full_name' => 'test',
                'email' => 'test@test.test',
                'password' => '$2y$10$0dzDmQwAtQLAYEc27AB4IepqWcao.7auDHpPmsG/FrulZIFLoXGza',
                'is_active' => '1',
                'created' => '2021-04-09 17:37:00',
                'modified' => '2021-04-09 17:37:00',
            ],
            [
                'id' => '11',
                'uuid' => '6070cc237ec40',
                'full_name' => 'aaaaaaass',
                'email' => 'aaa@aaa.aaa',
                'password' => '$2y$10$aJU1xSBojuTW20xx2HDV8e5qai9CbKjZw6j.WQUyYnXbLLw2g8DTK',
                'is_active' => '1',
                'created' => '2021-04-09 21:50:27',
                'modified' => '2021-04-09 21:50:50',
            ],
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
