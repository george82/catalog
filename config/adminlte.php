<?php
return [
    'Theme' => [
        'title' => 'CataLoG',
        'logo' => [
            'mini' => '<b>C</b>LG',
            'large' => '<b>Cata</b>LoG'
        ],
        'login' => [
            'show_remember' => false,
            'show_register' => false,
            'show_social' => false
        ],
        'folder' => ROOT,
        'skin' => 'blue'
    ]
];
