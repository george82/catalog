<?php

declare(strict_types=1);

namespace App\Controller\Admin;

use App\Controller\AppController;

use Cake\Filesystem\File;


class GalleryImagesController extends AppController
{
    # -------------------------------- 
    # Basic > BasicGalleryImages > Upload
    # --------------------------------
    public function upload($model, $foreignKey)
    {
        $galleryImage = $this->GalleryImages->newEmptyEntity();
        if ($this->request->is('post')) {

            $galleryImage = $this->GalleryImages->patchEntity($galleryImage, $this->request->getData());
            $galleryImage->foreign_key = $foreignKey;
            $galleryImage->model = $model;

            if ($this->GalleryImages->save($galleryImage)) {
                die();
            } else {
                debug($galleryImage);
                die('Error');
            }
        }
    }

    # Delete photos
    public function delete($id = null)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->request->allowMethod(['post', 'delete', 'put', 'get']);
        $galleryImage = $this->GalleryImages->get($id);

        $imagePath = new File(WWW_ROOT . $galleryImage->image_path);

        if ($this->GalleryImages->delete($galleryImage)) {
            # Delete the files
            $imagePath->delete();
            $this->Flash->success(__('The image has been deleted.'));
        } else {
            $this->Flash->error(__('The images could not be deleted. Please, try again.'));
        }
        return $this->redirect($this->referer());
        die();
    }

    /**
     * Edit method
     */
    public function edit($id = null)
    {
        $galleryImage = $this->GalleryImages->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $galleryImage = $this->GalleryImages->patchEntity($galleryImage, $this->request->getData());
            if ($this->GalleryImages->save($galleryImage)) {
                $this->Flash->success(__('The gallery image has been saved.'));

                return $this->redirect(['action' => 'edit', $id]);
            }
            $this->Flash->error(__('The gallery image could not be saved. Please, try again.'));
        }
        $this->set(compact('galleryImage'));
    }
}
