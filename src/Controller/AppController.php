<?php

declare(strict_types=1);

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\EventInterface;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/4/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('FormProtection');`
     *
     * @return void
     */


    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->loadComponent('Authentication.Authentication');

        /*
         * Enable the following component for recommended CakePHP form protection settings.
         * see https://book.cakephp.org/4/en/controllers/components/form-protection.html
         */
        //$this->loadComponent('FormProtection');
    }


    public function beforeFilter(EventInterface $event)
    {
        parent::beforeRender($event);

        if ($this->request->getParam('prefix') == 'Admin') {
            //$this->viewBuilder()->setLayout('default');
            $this->viewBuilder()->setTheme('AdminLTE');
            $this->viewBuilder()->setLayout('admin');
            // $this->viewBuilder()->setClassName('AdminLTE.AdminLTE');
            $this->_setAuthAdmin();
        } else {
            $this->viewBuilder()->setLayout('default');
            $this->_setAuthUser();
        }
    }


    private function _setAuthAdmin()
    {
        # We load the user
        $auth = $this->Authentication->getIdentity();

        if ($auth) {
            $this->loadModel('Users');
            try {
                // $this->authAdmin = $this->Admins->get($auth->id, ['contain' => ['AdminGroups']]);
                $this->authAdmin = $this->Users->get($auth->id);
            } catch (\Exception $e) {
                $this->Flash->error($e->getMessage());
                $this->Authentication->logout();
            }

            if (empty($this->authAdmin)) {
                return $this->redirect($this->referer());
            }
        } else {
            $this->authAdmin = null;
        }

        $this->set('authAdmin', $this->authAdmin);
    }
    private function _setAuthUser()
    {
        # We load the user
        $auth = $this->Authentication->getIdentity();

        if ($auth) {
            $this->loadModel('Users');
            try {
                // $this->authUser = $this->Users->get($auth->id, ['contain' => ['AdminGroups']]);
                $this->authUser = $this->Users->get($auth->id);
            } catch (\Exception $e) {
                $this->Flash->error($e->getMessage());
                $this->Authentication->logout();
            }

            if (empty($this->authUser)) {
                return $this->redirect($this->referer());
            }
        } else {
            $this->authUser = null;
        }

        $this->set('authUser', $this->authUser);
    }

    function setMessages(&$messages, $modelHumanName = 'entry')
    {
        $messages = (object) [
            'invalid' => __('Invalid {0}', $modelHumanName),
            'success' => __('The {0} has been saved.', $modelHumanName),
            'error' => __('The {0} could not be saved. Please, try again.', $modelHumanName),
            'success_delete' => __('The {0} has been deleted.', $modelHumanName),
            'error_delete' => __('The {0} could not be deleted. Please, try again.', $modelHumanName),
            'error_export' => __('Invalid request. Please access this page via Export button.'),
            'unauthorized' => __('You are not authorized to access this page.'),
        ];
    }
}
