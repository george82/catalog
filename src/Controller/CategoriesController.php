<?php

declare(strict_types=1);

namespace App\Controller;

use App\Controller\AppController;

/**
 * Categories Controller
 *
 * @method \App\Model\Entity\Category[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CategoriesController extends AppController
{

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['mainCategory', 'category', 'index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {

        // $categories = $this->paginate($this->Categories);
        $categories = $this->Categories->find()
            ->where([
                'Categories.parent_id IS NULL',
                'Categories.is_active' => true
            ])
            ->all();
        $this->set(compact('categories'));
    }

    /**
     * View method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */


    public function mainCategory($parentSlug)
    {
        $parentCategory = $this->Categories->find()
            ->where([
                'Categories.slug' => $parentSlug,
                'Categories.is_active' => true
            ])
            ->first();

        $categories = $this->Categories->find()
            ->where([
                'Parent_id' => $parentCategory['id'],
                'Categories.is_active' => true
            ])
            ->all();

        $this->loadModel('Products');

        $products = $this->Products->find()
            ->where(['Category_id' => $parentCategory['id']])
            ->all();

        // debug($categories);
        $this->set(compact('parentCategory', 'categories', 'products'));


        // if (!empty($parentCategory)) {

        //     if (!empty($parentCategory->custom_template)) {
        //         $this->set(compact('parentCategory'));
        //     } else {
        //         $this->loadModel('Products');
        //         $productIds = [];
        //         $products = NULL;
        //         $noProducts = FALSE;

        //         $conditions = [];
        //         $conditions['Products.category_id'] = $parentCategory->id;
        //         $conditions['Products.is_active'] = true;
        //         $getParams = $this->request->getQueryParams();
        //         $paramsCount = 0;
        //         if ($noProducts == FALSE) {
        //             $products = $this->Products->find()
        //                 ->contain(['Categories'])
        //                 ->order(['Products.price' => 'DESC'])
        //                 ->where($conditions)
        //                 ->all();
        //         }
        //         $this->set(compact('products', 'parentCategory'));
        //     }
        // } else {
        //     $this->Flash->error(__('Nu există aceasta categorie.'));
        //     return $this->redirect(['controller' => 'Pages', 'action' => 'index']);
        // }



    }

    public function category_old($slug = null)
    {

        // $this->loadModel('Categories');

        $category = $this->Categories->findBySlug($slug)->first();
        // $category = $this->Categories->findBySlug($slug)->firstOrFail();


        $query = $this->request->getQuery();
        $this->loadModel('Products');
        if (isset($query['sortare']) && !empty($query['sortare'])) {
            $sort =  $query['sortare'];
            if ($query['sortare'] == 'pret-crescator') {
                $products = $this->Products->find()
                    ->contain(['Categories'])
                    ->where(['Products.category_id' => $category->id,  'Products.is_active' => true])
                    ->order(['Products.price' => 'ASC'])
                    ->all();
            } else if ($query['sortare'] == 'pret-descrescator') {
                $products = $this->Products->find()
                    ->contain(['Categories'])
                    ->where(['Products.category_id' => $category->id,  'Products.is_active' => true])
                    ->order(['Products.price' => 'DESC'])
                    ->all();
            } else if ($query['sortare'] == 'nume-alfabetic') {
                $products = $this->Products->find()
                    ->contain(['Categories'])
                    ->where(['Products.category_id' => $category->id,  'Products.is_active' => true])
                    ->order(['Products.title' => 'ASC'])
                    ->all();
            } else if ($query['sortare'] == 'nume-invers-alfabetic') {
                $products = $this->Products->find()
                    ->contain(['Categories'])
                    ->where(['Products.category_id' => $category->id,  'Products.is_active' => true])
                    ->order(['Products.title' => 'DESC'])
                    ->all();
            }
            $this->set(compact('products', 'category', 'sort'));
        } else {
            $products = $this->Products->find()
                ->contain(['Categories'])
                ->where(['Products.category_id' => $category->id,  'Products.is_active' => true])
                ->all();
            $this->set(compact('products', 'category'));
        }
    }



    public function category($parentSlug, $slug)
    {

        $parentCategory = $this->Categories->find()
            ->where([
                'Categories.slug' => $parentSlug,
                'Categories.parent_id IS NULL',
                'Categories.is_active' => true
            ])
            ->first();

        //$category = $this->Categories->findBySlug($slug)->first();
        $category = $this->Categories->find()
            ->where([
                'Categories.slug' => $slug,
                'Categories.is_active' => true
            ])
            ->first();

        if (!empty($parentCategory)) {
            $productIds = [];
            $products = NULL;
            $noProducts = FALSE;

            if (!empty($category)) {
                $query = $this->request->getQuery();
                $this->loadModel('Products');
                $conditions = [];
                $conditions['Products.category_id'] = $category->id;
                $conditions['Products.is_active'] = true;

                if (isset($query['sortare']) && !empty($query['sortare'])) {
                    $sort =  $query['sortare'];
                    if ($query['sortare'] == 'pret-crescator') {
                        $products = $this->Products->find()
                            ->contain(['Categories'])
                            ->where(['Products.category_id' => $category->id,  'Products.is_active' => true])
                            ->order(['Products.price' => 'ASC'])
                            ->all();
                    } else if ($query['sortare'] == 'pret-descrescator') {
                        $products = $this->Products->find()
                            ->contain(['Categories'])
                            ->where(['Products.category_id' => $category->id,  'Products.is_active' => true])
                            ->order(['Products.price' => 'DESC'])
                            ->all();
                    } else if ($query['sortare'] == 'nume-alfabetic') {
                        $products = $this->Products->find()
                            ->contain(['Categories'])
                            ->where(['Products.category_id' => $category->id,  'Products.is_active' => true])
                            ->order(['Products.title' => 'ASC'])
                            ->all();
                    } else if ($query['sortare'] == 'nume-invers-alfabetic') {
                        $products = $this->Products->find()
                            ->contain(['Categories'])
                            ->where(['Products.category_id' => $category->id,  'Products.is_active' => true])
                            ->order(['Products.title' => 'DESC'])
                            ->all();
                    }
                    $this->set(compact('products', 'category', 'parentCategory', 'sort'));
                } else {
                    $products = $this->Products->find()
                        ->contain(['Categories'])
                        ->where(['Products.category_id' => $category->id,  'Products.is_active' => true])
                        ->all();
                    $this->set(compact('products', 'category', 'parentCategory'));
                }
            } else {
                $this->Flash->error(__('Nu există această categorie.'));
                return $this->redirect(['controller' => 'Categories', 'action' => 'mainCategory', $parentCategory->slug]);
            }
        } else {
            $this->Flash->error(__('Nu exista aceasta categorie.'));
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        }
    }

    public function product($slug = null)
    {
        $this->loadModel('Products');
        $product = $this->Products->findBySlug($slug)->firstOrFail();


        $this->set(compact('product'));
    }
}
