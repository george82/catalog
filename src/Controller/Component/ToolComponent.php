<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

class ToolComponent extends Component
{
    public function generateUuid()
    {
        $uuid = uniqid();
        return $uuid;
    }

    /*
    * create upload
    */
    public function uploadFile($image, $folderImage)
    {
        $image_name = $image->getClientFilename();
        $image_type = $image->getClientMediaType();
        $image_size = $image->getSize();
        $ext = pathinfo($image_name, PATHINFO_EXTENSION);

        if ($image_name) {
            $image_name = date("Y_m_d_") . $this->generateUuid(). '.' . $ext;
            $target_path = WWW_ROOT . 'img/' . $folderImage . '/' . $image_name;
            $image->moveTo($target_path);

            return $image_name;
        } else {
            return;
        }
    }
}
