<?php

declare(strict_types=1);

namespace App\Controller;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{


    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['view']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentProducts', 'Categories'],
        ];
        $products = $this->paginate($this->Products);

        $this->set(compact('products'));
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view_old($slug = null)
    {
        // $product = $this->Products->get($id, [
        //     'contain' => ['ParentProducts', 'Categories', 'ChildProducts'],
        // ]);
        $product = $this->Products->findBySlug($slug)->firstOrFail();
        $this->set(compact('product'));
    }
    public function view()
    {

        $this->loadModel('Categories');

        $conditions = [];
        $conditions = ['Products.is_active' => true];
        // $parentCategory = $this->request->getParam('parentSlugCat');
        $parentCategory = $this->Categories->find()
            ->where([
                'Categories.slug' => $this->request->getParam('parentSlugCat'),
                'Categories.is_active' => true
            ])
            ->first();


        // $category = $this->request->getParam('slugCat');
        if (!empty($this->request->getParam('slugCat'))) {
            $category  = $this->Categories->find()
                ->where([
                    'Categories.slug' => $this->request->getParam('slugCat'),
                    'Categories.is_active' => true
                ])
                ->first();
        } else {
            $category = [];
        }
        $slug = $this->request->getParam('slug');

        $product = $this->Products->findBySlug($slug)
            ->contain([
                'ParentProducts',
                'Categories',
                'GalleryImages'
            ])
            ->where($conditions)
            ->first();

        if ($product) {
            if ($product->slug != $slug) {
                if (!empty($category->slug)) {
                    return $this->redirect(['controller' => 'Products', 'action' => 'view', 'parentSlugCat' => $parentCategory->slug, 'slugCat' => $category->slug, 'slug' => $product->slug], 301);
                } else {
                    return $this->redirect(['controller' => 'Products', 'action' => 'view', 'parentSlugCat' => $parentCategory->slug, 'slug' => $product->slug], 301);
                }
            }
            $this->set(compact('product', 'parentCategory', 'category'));
        } else {
            $this->Flash->error(__('Acest produs nu a fost găsit.'));
            if (!empty($category->slug)) {
                return $this->redirect(['controller' => 'Categories', 'action' => 'category', 'parentSlug' => $parentCategory->slug, 'slug' => $category->slug]);
            } else {
                if (!empty($parentCategory->slug)) {
                    return $this->redirect(['controller' => 'Categories', 'action' => 'mainCategory', 'parentSlug' => $parentCategory->slug]);
                } else {
                    return $this->redirect(['controller' => 'Pages', 'action' => 'index']);
                    $this->Flash->error(__('Acest produs nu a fost găsit.'));
                }
            }
        }
    }
}
