<?php

declare(strict_types=1);

namespace App\Controller;

/**
 * Products Controller
 */
class SearchesController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['index']);
    }
    public function index()
    {
        $query = $this->request->getQuery();
        $results = [];
        $q = [];
        if (!empty($query['q'])) {
            $q = trim($query['q']);
            $this->loadModel('Categories');
            $this->loadModel('Products');

            $results['categories'] =  $this->Categories->find()
                ->contain(['ParentCategories'])
                ->where([
                    'OR' => [
                        ['Categories.title LIKE' => '%' . $q . '%'],
                        ['Categories.description LIKE' => '%' . $q . '%']
                    ],
                ])
                ->toArray();
            $results['products'] = $this->Products->find()
                ->contain(['Categories', 'Categories.ParentCategories'])
                ->where([
                    'OR' => [
                        ['Products.title LIKE' => '%' . $q . '%'],
                        ['Products.description LIKE' => '%' . $q . '%'],
                        ['Products.keywords LIKE' => '%' . $q . '%']
                    ],
                ])
                ->toArray();
        }

        // insert count results
        $displayedResults = 0;
        if ($results) {
            foreach ($results as $k => $items) {
                foreach ($items as $item) {
                    if (!empty($item)) {
                        $displayedResults++;
                    };
                }
            }
        }

        $data = [
            'count_last_displayed' => $displayedResults,

        ];
        $this->set(compact('results', 'q', 'displayedResults'));
    }
}
