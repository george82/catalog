<?php

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\ORM\Behavior;
use Cake\Filesystem\Folder;
use Cake\Utility\Security;

class UploadFileBehavior extends Behavior
{

    protected $_defaultConfig = [
        'fieldName'             =>  'file',
        'databaseFieldName'     =>  'file_path',
        'isPrivate'             =>  false,
        'keepName'              =>  false
    ];

    private $fields, $path, $pathRelative;


    # --------------------------------
    # beforeMarshal
    # --------------------------------
    public function beforeMarshal(EventInterface $event, ArrayObject $data, ArrayObject $options)
    {
        $defaultConfig = $this->getConfig();

        # If we have multiple fields
        if (isset($defaultConfig['fields'])) {
            foreach ($defaultConfig['fields'] as $field) {
                if (!isset($field['fieldName']) || !isset($defaultConfig['databaseFieldName'])) {
                    continue;
                } else {
                    $this->fields[$field['fieldName']] = $field;
                    $this->fields[$field['fieldName']]['baseFilename'] = Security::randomString(16);
                    if (!isset($field['sizes'])) {
                        $this->fields[$field['fieldName']]['sizes'] = $defaultConfig['sizes'];
                    }
                }
            }
        } else {
            # If only one    
            $this->fields[$defaultConfig['fieldName']] = $defaultConfig;
            $this->fields[$defaultConfig['fieldName']]['baseFilename'] = Security::randomString(16);
        }

        $this->pathRelative = 'uploads' . DS . 'files' . DS  . date('Y') . DS . date('m');
        if ($defaultConfig['isPrivate']) {
            $this->path = ROOT . DS . 'resources' .   DS  . $this->pathRelative;
        } else {
            $this->path = ROOT . DS . 'webroot' .   DS  . $this->pathRelative;
        }



        // We create the folder
        $mkdir = new Folder($this->path, true, 0755);

        foreach ($this->fields as $fieldName => $fieldOptions) {

            if (isset($data[$fieldName])) {
                $originalFile = $data[$fieldName];
                if (!empty($originalFile) && $originalFile->getClientFilename()) {

                    // We upload the original field
                    $data[$fieldOptions['databaseFieldName']] = $this->_uploadFile($originalFile, $fieldOptions);
                }
            }
        }
    }



    # --------------------------------
    # upload file
    # --------------------------------
    public function _uploadFile($originalFile = null, $fieldOptions = [])
    {

        # If the fils is null
        if ($originalFile == null) {
            return null;
        }


        # We get image info and sizes
        $parts = pathinfo($originalFile->getClientFilename());
        $extension = $parts['extension'];


        // We generate the file name
        $fileSave = $this->path . DS . $fieldOptions['baseFilename'] . '.' . $extension;


        if (move_uploaded_file($originalFile->getStream()->getMetadata('uri'), $fileSave)) {
            return $this->pathRelative . DS . $fieldOptions['baseFilename'] . '.' . $extension;
        } else {
            return null;
        }
    }
}
