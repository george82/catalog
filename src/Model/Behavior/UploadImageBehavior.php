<?php

namespace App\Model\Behavior;

use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\EventInterface;
use Cake\ORM\Behavior;
use Cake\Filesystem\Folder;
use Cake\Utility\Security;
use Imagine\Image\Box;
use Imagine\Gd\Imagine;


class UploadImageBehavior extends Behavior
{


    # --------------------------------
    # config
    # [sizes = [400,600,800,1200]]
    # --------------------------------
    protected $_defaultConfig = [
        'fieldName'         =>  'file',
        'databaseFieldName' =>  'image_path',
        // 'sizes'             =>  ['original', 1920, 1400, 800, 500, 120],
        'sizes'             =>  ['original', 500],
    ];

    private $fields, $path, $pathRelative, $baseFilename;



    # --------------------------------
    # beforeMarshal
    # --------------------------------
    public function beforeMarshal(EventInterface $event, ArrayObject $data, ArrayObject $options)
    {

        $defaultConfig = $this->getConfig();

        # If we have multiple fields
        if (isset($defaultConfig['fields'])) {
            foreach ($defaultConfig['fields'] as $field) {
                if (!isset($field['fieldName']) || !isset($defaultConfig['databaseFieldName'])) {
                    continue;
                } else {
                    $this->fields[$field['fieldName']] = $field;
                    $this->fields[$field['fieldName']]['baseFilename'] = Security::randomString(16);
                    if (!isset($field['sizes'])) {
                        $this->fields[$field['fieldName']]['sizes'] = $defaultConfig['sizes'];
                    }
                }
            }
        } else {
            # If only one    
            $this->fields[$defaultConfig['fieldName']] = $defaultConfig;
            $this->fields[$defaultConfig['fieldName']]['baseFilename'] = Security::randomString(16);
        }

        $this->pathRelative = 'uploads' . DS  . date('Y') . DS . date('m');
        $this->path = ROOT . DS . 'webroot' .   DS  . $this->pathRelative;


        // We create the folder
        $mkdir = new Folder($this->path, true, 0755);

        foreach ($this->fields as $fieldName => $fieldOptions) {

            if (isset($data[$fieldName])) {
                $originalFile = $data[$fieldName];
                if (!empty($originalFile) && $originalFile->getClientFilename()) {

                    // We upload the original field
                    $data[$fieldOptions['databaseFieldName']] = $this->_uploadImage($originalFile, $fieldOptions);
                }
            }
        }
    }

    private function _uploadImage($originalFile, $fieldOptions)
    {
        # We create the new image
        $imagine = new Imagine();

        # We get image info and sizes
        $parts = pathinfo($originalFile->getClientFilename());
        $extension = $parts['extension'];
        if ($extension == 'jpeg') {
            $extension = 'jpg';
        }
        list($originalWidth, $originalHeight) = getimagesize($originalFile->getStream()->getMetadata('uri'));
        $originalRatio = $originalHeight / $originalWidth;


        # We open the image
        $newImage = $imagine->open($originalFile->getStream()->getMetadata('uri'));

        # We go through all sizes
        foreach ($fieldOptions['sizes'] as $size) {
            if ($size == 'original') {
                $pathForSize = $this->path . DS . $fieldOptions['baseFilename'];
                switch ($extension) {
                    case 'jpg':
                        $newImage
                            ->save($pathForSize . '-original.jpg', array('jpeg_quality' => 100));

                        # We save to Webp as well
                        imagewebp(imagecreatefromjpeg($pathForSize . '-original.jpg'), $pathForSize . '.webp');
                        break;

                    case 'png':

                        $newImage
                            ->save($pathForSize . '-original.png', array('png_compression_level' => 9));

                        # We save to Webp as well
                        imagewebp(imagecreatefrompng($pathForSize . '-original.png'), $pathForSize . '.webp');

                        break;
                }
                $path = $this->pathRelative . DS . $fieldOptions['baseFilename'] . '-original.' . $extension;
            } else {
                //if ($size <= $originalWidth) {
                $width = $size;
                $height = $size * $originalRatio;
                $pathForSize = $this->path . DS . $fieldOptions['baseFilename'] . '-' . $size;
                $newImage->resize(new Box($width, $height));
                switch ($extension) {
                    case 'jpg':
                        $newImage
                            ->save($pathForSize . '.jpg', array('jpeg_quality' => 100));

                        # We save to Webp as well
                        imagewebp(imagecreatefromjpeg($pathForSize . '.jpg'), $pathForSize . '.webp');
                        break;

                    case 'png':

                        $newImage
                            ->save($pathForSize . '.png', array('png_compression_level' => 9));

                        # We save to Webp as well
                        imagewebp(imagecreatefrompng($pathForSize . '.png'), $pathForSize . '.webp');

                        break;
                }
                //}
            }
        }

        return '/' . str_replace(DS, '/', $path);
    }
}
