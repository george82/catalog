<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GalleryImage Entity
 */
class GalleryImage extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     */
    protected $_accessible = [
        '*' => true,
    ];
}
