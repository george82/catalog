<?php

declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property string $uuid
 * @property int|null $parent_id
 * @property int $category_id
 * @property string $title
 * @property string $description
 * @property string $slug
 * @property string|null $featured_image
 * @property string $keywords
 * @property bool $is_active
 * @property int $lft
 * @property int $rght
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Product $parent_product
 * @property \App\Model\Entity\Category $category
 * @property \App\Model\Entity\Color $color
 * @property \App\Model\Entity\Product[] $child_products
 */
class Product extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    // protected $_accessible = [
    //     'uuid' => true,
    //     'parent_id' => true,
    //     'category_id' => true,
    //     'title' => true,
    //     'description' => true,
    //     'price' => true,
    //     'slug' => true,
    //     'featured_image' => true,
    //     'keywords' => true,
    //     'is_active' => true,
    //     'lft' => true,
    //     'rght' => true,
    //     'created' => true,
    //     'modified' => true,
    //     'parent_product' => true,
    //     'category' => true,
    //     'color' => true,
    //     'child_products' => true,
    // ];

    protected $_accessible = [
        '*' => true
    ];
}
