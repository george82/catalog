<?php

namespace App\View\Helper;

use Cake\View\Helper;

class CustomHelper extends Helper
{

    public $helpers = ['Html', 'Thumb'];

    public function image($path, $size = null, $options = null)
    {
        if (isset($options['webp']) && $options['webp'] != false) {
            $path = str_replace(['-original.jpeg', '-original.jpg', '-original.png'], '-original.webp', $path);
            unset($options['webp']);
        }
        if ($size) {
            $path = str_replace('-original', '-' . $size, $path);
        }
        return $this->Html->image($path, $options);
    }



    public function imageResize($path = [], $size = [], $options = null)
    {
        if ($path['model_name'] == 'Recipes') {
            $pathImg = WWW_ROOT . UPLOAD_IMAGE . $path['model_name'] .  '/'  . $path['featured_image'];
        } else {
            $pathImg = WWW_ROOT .  $path['featured_image'];
        }
        if (file_exists($pathImg)) {
            return  $this->Thumb->resize($pathImg, ['width' => $size['width'], 'height' => $size['height'], 'format' => $size['format']]);
        } else {
            return $this->Thumb->resize('no-image.png', ['width' => $size['width'], 'height' => $size['height'], 'format' => $size['format']]);
        }
    }

    public function imageResizeUrl($path = [], $size = [], $options = null)
    {
        if ($path['model_name'] == 'Recipes') {
            $pathImg = WWW_ROOT . UPLOAD_IMAGE . $path['model_name'] .  '/'  . $path['featured_image'];
        } else {
            $pathImg = WWW_ROOT .  $path['featured_image'];
        }
        if (file_exists($pathImg)) {
            return  $this->Thumb->resizeUrl($pathImg, ['width' => $size['width'], 'height' => $size['height'], 'format' => $size['format']]);
        } else {
            return $this->Thumb->resizeUrl('no-image.png', ['width' => $size['width'], 'height' => $size['height'], 'format' => $size['format']]);
        }
    }
}
