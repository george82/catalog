<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Category
    <small><?php echo __('Add'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo __('Form'); ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo $this->Form->create($category, ['role' => 'form', 'type' => 'file']); ?>
        <div class="box-body">
          <div class="container ml-0 mr-auto">
            <div class="row">
              <div class="col-12 col-sm-6 col-md-6">
                <?php

                echo $this->Form->control('title');
                echo $this->Form->control('slug');


                echo $this->Form->control('is_active');
                ?>
              </div>
              <div class="col-12 col-sm-6 col-md-6">
                <?php
                echo $this->Form->control('parent_id', ['options' => $parentCategories, 'empty' => true]);
                echo $this->Form->control('file', ['label' => 'Featured Image', 'type' => 'file']);
                ?>
              </div>
              <div class="col-12 col-sm-12 col-md-12">
                <?php
                echo $this->Form->control('description');
                ?>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->

        <?php echo $this->Form->submit(__('Submit')); ?>

        <?php echo $this->Form->end(); ?>
      </div>
      <!-- /.box -->
    </div>
  </div>
  <!-- /.row -->
</section>

<?php $this->start('scriptBottom'); ?>
<script>

</script>
<?php $this->end(); ?>