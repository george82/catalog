<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Categories

    <div class="pull-right"><?php echo $this->Html->link(__('New'), ['action' => 'add'], ['class' => 'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?php echo __('List'); ?></h3>

          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm" style="width: 150px;">
                <input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                <div class="input-group-btn">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('parent_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('slug') ?></th>
                <th scope="col"><?= $this->Paginator->sort('featured_image') ?></th>
                <th scope="col"><?= $this->Paginator->sort('description') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_active') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($categories as $category) : ?>
                <tr>
                  <td><?= $this->Number->format($category->id) ?></td>
                  <td>
                    <?php # echo $this->Number->format($category->parent_id) 
                    ?>
                    <?php echo ($category->parent_id) ? $category->parent_category->title : 'NA' ?>
                  </td>
                  <td><?= h($category->title) ?></td>
                  <td><?= h($category->slug) ?></td>
                  <td>
                    <?php # echo h($category->featured_image) 
                    ?>

                    <?php
                    if (!empty($category->featured_image)) {
                      $image = $this->Thumb->fit('..' . $category->featured_image,  [
                        'width' => 100,
                        'height' => 100,
                        'format' => 'jpg',
                        'quality' => '70'
                      ]);
                    } else {
                      $image = $this->Html->image('no-image.jpg', ['width' => '100']);
                    }
                    echo $image;
                    ?>

                  </td>
                  <td><?= strip_tags($category->description) ?></td>
                  <td>
                    <?php if ($category->is_active == 1) {
                      echo '<span style="color: green;">✔</span>';
                    } else {
                      echo '<span style="color: red;">✖</span>';
                    } ?>
                  </td>
                  <td><?= h($category->created) ?></td>
                  <td><?= h($category->modified) ?></td>
                  <td class="actions text-right">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $category->id], ['class' => 'btn btn-info btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $category->id], ['class' => 'btn btn-warning btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id), 'class' => 'btn btn-danger btn-xs']) ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>