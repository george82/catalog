<section class="content-header">
  <h1>
    Category
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Parent Category') ?></dt>
            <dd><?= $category->has('parent_category') ? $this->Html->link($category->parent_category->title, ['controller' => 'Categories', 'action' => 'view', $category->parent_category->id]) : '' ?></dd>
            <dt scope="row"><?= __('Title') ?></dt>
            <dd><?= h($category->title) ?></dd>
            <dt scope="row"><?= __('Slug') ?></dt>
            <dd><?= h($category->slug) ?></dd>
            <dt scope="row"><?= __('Featured Image') ?></dt>
            <dd><?= h($category->featured_image) ?></dd>
            <dt scope="row"><?= __('Description') ?></dt>
            <dd><?= h($category->description) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($category->id) ?></dd>
            <dt scope="row"><?= __('Lft') ?></dt>
            <dd><?= $this->Number->format($category->lft) ?></dd>
            <dt scope="row"><?= __('Rght') ?></dt>
            <dd><?= $this->Number->format($category->rght) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($category->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($category->modified) ?></dd>
            <dt scope="row"><?= __('Is Active') ?></dt>
            <dd><?= $category->is_active ? __('Yes') : __('No'); ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Categories') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($category->child_categories)): ?>
          <table class="table table-hover">
              <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Parent Id') ?></th>
                    <th scope="col"><?= __('Title') ?></th>
                    <th scope="col"><?= __('Slug') ?></th>
                    <th scope="col"><?= __('Featured Image') ?></th>
                    <th scope="col"><?= __('Description') ?></th>
                    <th scope="col"><?= __('Is Active') ?></th>
                    <th scope="col"><?= __('Lft') ?></th>
                    <th scope="col"><?= __('Rght') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
              <?php foreach ($category->child_categories as $childCategories): ?>
              <tr>
                    <td><?= h($childCategories->id) ?></td>
                    <td><?= h($childCategories->parent_id) ?></td>
                    <td><?= h($childCategories->title) ?></td>
                    <td><?= h($childCategories->slug) ?></td>
                    <td><?= h($childCategories->featured_image) ?></td>
                    <td><?= h($childCategories->description) ?></td>
                    <td><?= h($childCategories->is_active) ?></td>
                    <td><?= h($childCategories->lft) ?></td>
                    <td><?= h($childCategories->rght) ?></td>
                    <td><?= h($childCategories->created) ?></td>
                    <td><?= h($childCategories->modified) ?></td>
                      <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['controller' => 'Categories', 'action' => 'view', $childCategories->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['controller' => 'Categories', 'action' => 'edit', $childCategories->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['controller' => 'Categories', 'action' => 'delete', $childCategories->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childCategories->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
              </tr>
              <?php endforeach; ?>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Products') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($category->products)): ?>
          <table class="table table-hover">
              <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Uuid') ?></th>
                    <th scope="col"><?= __('Parent Id') ?></th>
                    <th scope="col"><?= __('Category Id') ?></th>
                    <th scope="col"><?= __('Title') ?></th>
                    <th scope="col"><?= __('Price') ?></th>
                    <th scope="col"><?= __('Description') ?></th>
                    <th scope="col"><?= __('Slug') ?></th>
                    <th scope="col"><?= __('Featured Image') ?></th>
                    <th scope="col"><?= __('Keywords') ?></th>
                    <th scope="col"><?= __('Is Active') ?></th>
                    <th scope="col"><?= __('Lft') ?></th>
                    <th scope="col"><?= __('Rght') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
              <?php foreach ($category->products as $products): ?>
              <tr>
                    <td><?= h($products->id) ?></td>
                    <td><?= h($products->uuid) ?></td>
                    <td><?= h($products->parent_id) ?></td>
                    <td><?= h($products->category_id) ?></td>
                    <td><?= h($products->title) ?></td>
                    <td><?= h($products->price) ?></td>
                    <td><?= h($products->description) ?></td>
                    <td><?= h($products->slug) ?></td>
                    <td><?= h($products->featured_image) ?></td>
                    <td><?= h($products->keywords) ?></td>
                    <td><?= h($products->is_active) ?></td>
                    <td><?= h($products->lft) ?></td>
                    <td><?= h($products->rght) ?></td>
                    <td><?= h($products->created) ?></td>
                    <td><?= h($products->modified) ?></td>
                      <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['controller' => 'Products', 'action' => 'edit', $products->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['controller' => 'Products', 'action' => 'delete', $products->id], ['confirm' => __('Are you sure you want to delete # {0}?', $products->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
              </tr>
              <?php endforeach; ?>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
