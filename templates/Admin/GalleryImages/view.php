<section class="content-header">
  <h1>
    Gallery Image
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Model') ?></dt>
            <dd><?= h($galleryImage->model) ?></dd>
            <dt scope="row"><?= __('Title') ?></dt>
            <dd><?= h($galleryImage->title) ?></dd>
            <dt scope="row"><?= __('Image Path') ?></dt>
            <dd><?= h($galleryImage->image_path) ?></dd>
            <dt scope="row"><?= __('Video Code') ?></dt>
            <dd><?= h($galleryImage->video_code) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($galleryImage->id) ?></dd>
            <dt scope="row"><?= __('Foreign Key') ?></dt>
            <dd><?= $this->Number->format($galleryImage->foreign_key) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($galleryImage->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($galleryImage->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
