<?php

/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Product
    <small><?php echo __('Edit'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo __('Form'); ?></h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <?php echo $this->Form->create($product, ['role' => 'form', 'type' => 'file']); ?>
        <div class="box-body">
          <div class="container ml-0 mr-auto">
            <div class="row">
              <div class="col-12 col-sm-6 col-md-6">
                <?php
                echo $this->Form->control('uuid', ['type' => 'hidden']);
                ?>
                <?php
                echo $this->Form->control('title');
                ?>

                <?php
                echo $this->Form->control('parent_id', ['options' => $parentProducts, 'empty' => true]);
                ?>
                <?php
                echo $this->Form->control('category_id', ['options' => $categories]);
                ?>
                <?php
                echo $this->Form->control('keywords');
                ?>
              </div>
              <div class="col-12 col-sm-6 col-md-6">
                <?php
                echo $this->Form->control('slug');
                ?>
                <?php
                echo $this->Form->control('price');
                // echo $this->Form->control('featured_image');
                ?>
                <?php
                echo $this->Form->control('file', ['label' => 'Featured Image', 'type' => 'file']);
                ?>

                <?php
                echo $this->Form->control('is_active');
                ?>
              </div>
              <div class="col-12 col-sm-12 col-md-12">
                <?php
                echo $this->Form->control('description');
                ?>
              </div>
            </div>
          </div>
          <!-- /.box-body -->

          <?php echo $this->Form->submit(__('Submit')); ?>
        </div>
        <?php echo $this->Form->end(); ?>
        <!-- /.box -->
      </div>
    </div>
    <!-- /.row -->
</section>