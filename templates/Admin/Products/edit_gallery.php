<section class="content-header">
    <h1>
        <?php echo __('Edit Gallery: ') . $product->title; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo __('Form'); ?></h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->

                <div class="box-body">
                    <div class="container ml-0 mr-auto">
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo $this->Element('GalleryImages/dropzone', ['foreignKey' => $product->id, 'model' => $model]) ?>
                            </div>
                            <div class="col-md-12">
                                <?php if ($images) { ?>
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th scope="col"><?php echo $this->Paginator->sort('image_path', ['label' => _('Image')]); ?></th>
                                                <th scope="col"><?php echo $this->Paginator->sort('title'); ?></th>
                                                <th scope="col"><?php echo $this->Paginator->sort('video_code'); ?></th>
                                                <th scope="col" class="text-right"><?php echo __('Actions'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($images as $image) : ?>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        echo ($image->image_path) ? $this->Html->image($image->image_path, array('width' => '100')) : '<i class="fa fa-image"></i>';
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php echo h($image->title); ?>
                                                    </td>
                                                    <td>
                                                        <?php echo h($image->video_code);
                                                        ?>
                                                    </td>
                                                    <td class="text-right">
                                                        <?php echo $this->Html->link(__('Edit'), ['prefix' => 'Admin', 'controller' => 'GalleryImages', 'action' => 'edit', $image->id], ['class' => 'btn btn-warning btn-xs']); ?>
                                                        <?php echo $this->Form->postLink(__('Delete'), ['controller' => 'GalleryImages', 'action' => 'delete', $image->id], ['confirm' => __('Are you sure you want to delete # {0}?', $image->title), 'class' => 'btn btn-danger btn-xs']); ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                <?php } else { ?>
                                    No images

                                <?php }  ?>
                            </div>

                        </div>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.row -->
</section>