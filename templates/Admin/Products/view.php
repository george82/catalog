<section class="content-header">
  <h1>
    Product
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Uuid') ?></dt>
            <dd><?= h($product->uuid) ?></dd>
            <dt scope="row"><?= __('Parent Product') ?></dt>
            <dd><?= $product->has('parent_product') ? $this->Html->link($product->parent_product->title, ['controller' => 'Products', 'action' => 'view', $product->parent_product->id]) : '' ?></dd>
            <dt scope="row"><?= __('Category') ?></dt>
            <dd><?= $product->has('category') ? $this->Html->link($product->category->title, ['controller' => 'Categories', 'action' => 'view', $product->category->id]) : '' ?></dd>
            <dt scope="row"><?= __('Title') ?></dt>
            <dd><?= h($product->title) ?></dd>

            <dt scope="row"><?= __('Price') ?></dt>
            <dd><?= h($product->price) ?></dd>
            <dt scope="row"><?= __('Slug') ?></dt>
            <dd><?= h($product->slug) ?></dd>
            <dt scope="row"><?= __('Featured Image') ?></dt>

            <dd>

              <?php
              if (!empty($product->featured_image)) {
                $image = $this->Thumb->fit('..' . $product->featured_image,  [
                  'width' => 100,
                  'height' => 100,
                  'format' => 'jpg',
                  'quality' => '70'
                ]);
              } else {
                $image = $this->Html->image('no-image.jpg', ['width' => '100']);
              }
              echo $image;
              ?>
            </dd>


            <dt scope="row"><?= __('Keywords') ?></dt>
            <dd><?= h($product->keywords) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($product->id) ?></dd>
            <dt scope="row"><?= __('Lft') ?></dt>
            <dd><?= $this->Number->format($product->lft) ?></dd>
            <dt scope="row"><?= __('Rght') ?></dt>
            <dd><?= $this->Number->format($product->rght) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($product->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($product->modified) ?></dd>
            <dt scope="row"><?= __('Is Active') ?></dt>
            <dd><?= $product->is_active ? __('Yes') : __('No'); ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Description') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?= $this->Text->autoParagraph($product->description); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Products') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($product->child_products)) : ?>
            <table class="table table-hover">
              <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Uuid') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Category Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Featured Image') ?></th>
                <th scope="col"><?= __('Keywords') ?></th>
                <th scope="col"><?= __('Is Active') ?></th>
                <th scope="col"><?= __('Lft') ?></th>
                <th scope="col"><?= __('Rght') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
              <?php foreach ($product->child_products as $childProducts) : ?>
                <tr>
                  <td><?= h($childProducts->id) ?></td>
                  <td><?= h($childProducts->uuid) ?></td>
                  <td><?= h($childProducts->parent_id) ?></td>
                  <td><?= h($childProducts->category_id) ?></td>
                  <td><?= h($childProducts->title) ?></td>
                  <td><?= h($childProducts->description) ?></td>
                  <td><?= h($childProducts->slug) ?></td>
                  <td><?= h($childProducts->featured_image) ?></td>
                  <td><?= h($childProducts->keywords) ?></td>
                  <td><?= h($childProducts->is_active) ?></td>
                  <td><?= h($childProducts->lft) ?></td>
                  <td><?= h($childProducts->rght) ?></td>
                  <td><?= h($childProducts->created) ?></td>
                  <td><?= h($childProducts->modified) ?></td>
                  <td class="actions text-right">
                    <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $childProducts->id], ['class' => 'btn btn-info btn-xs']) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Products', 'action' => 'edit', $childProducts->id], ['class' => 'btn btn-warning btn-xs']) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Products', 'action' => 'delete', $childProducts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childProducts->id), 'class' => 'btn btn-danger btn-xs']) ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>