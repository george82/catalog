<?php
// $this->layout = "login";
/**
 * @var \App\View\AppView $this
 */
?>
<div class="users form content">
    <?= $this->Form->create() ?>
    <fieldset>
        <?= $this->Form->control('email') ?>
        <?= $this->Form->control('password', ['label' => 'Parolă']) ?>
    </fieldset>
    <div class="text-center">
        <?= $this->Form->button(__('Autentificare')); ?>
    </div>


    <?= $this->Form->end() ?>
</div>