<?php echo $this->Form->create(null, ['type' => 'file', 'class' => 'dropzone', 'id' => 'galleryDropzone', 'url' => [
    'prefix' => 'Admin',
    'controller' => 'GalleryImages',
    'action' => 'upload',
    $model,
    $foreignKey
]]); ?>
<div class="fallback">
    <input name="file" type="file" multiple />
</div>
<?php echo $this->Form->end(); ?>