<?php $this->assign('title', __('Categorii')); ?>




<div class="container">
    <div class="row">
        <div class="col-12">
            <?php
            $this->Breadcrumbs->reset()->add($this->fetch('title'), NULL);
            $this->Breadcrumbs->prepend([
                ['title' => __('Acasă'), 'url' => ['controller' => 'Pages', 'action' => 'index']],
            ]);
            echo $this->Breadcrumbs->render(
                ['class' => 'breadcrumbs-trail'],
                ['separator' => '/']
            );
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <?php # echo $this->element('front/slider'); 
            ?>
        </div>
    </div>
    <?php if (count($categories)) { ?>
        <div class="row">
            <?php foreach ($categories as $category) { ?>
                <div class="col-6 col-md-3 mb-5 d-flex align-items-stretch">
                    <div class="card">
                        <div class="card-image">
                            <?php
                            if (!empty($category->featured_image)) {
                                $image = $this->Thumb->fit('..' . $category->featured_image,  [
                                    'width' => 300,
                                    'height' => 200,
                                    'format' => 'jpg',
                                    'quality' => 75
                                ]);
                            } else {
                                $image = $this->Html->image('no-image.jpg', ['class' => 'card-img-top']);
                            }
                            echo $image;
                            ?>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">
                                <?php echo strip_tags($category->title); ?>
                            </h5>
                            <p class="card-text">
                                <?php echo strip_tags($category->description); ?>
                            </p>

                            <?php echo $this->Html->link('Vezi mai multe', ['controller' => 'Categories', 'action' => 'mainCategory', $category->slug], ['class' => 'btn btn-primary stretched-link']) ?>

                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class="row">
            <div class="col-12">
                <h3>
                    Nu avem subcategorii.
                </h3>
            </div>
        </div>
    <?php }  ?>
</div>