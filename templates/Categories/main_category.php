<?php

use App\Model\Entity\Category;

$this->assign('title', $parentCategory->title);

?>
<div class="jumbotron">
    <div class="jumbotron-image">
        <?php
        if (!empty($parentCategory->featured_image)) {
            $image = $this->Thumb->fit('..' . $parentCategory->featured_image,  [
                'width' => 1400,
                'height' => 400,
                'format' => 'jpg',
                'quality' => '80'
            ]);
        } else {
            $image = $this->Html->image('no-image.jpg', ['class' => '']);
        }
        echo $image;
        ?>
    </div>
    <div class="jumbotron-text">
        <div class="container">
            <div class="row">
                <div class="col-12 my-5">
                    <div class="mb-2">
                        <h2>
                            <?php echo $this->fetch('title'); ?>
                        </h2>
                    </div>
                    <div class="mb-2">
                        <p>
                            <?php echo strip_tags($parentCategory->description); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-12">
            <?php
            $this->Breadcrumbs->reset()->add($this->fetch('title'), NULL);
            $this->Breadcrumbs->prepend([
                ['title' => __('Categorii'), 'url' => ['controller' => 'Categories', 'action' => 'index']],
            ]);
            $this->Breadcrumbs->prepend([
                ['title' => __('Acasă'), 'url' => ['controller' => 'Pages', 'action' => 'index']],
            ]);

            echo $this->Breadcrumbs->render(
                ['class' => 'breadcrumbs-trail'],
                ['separator' => '/']
            );
            ?>
        </div>
    </div>
    <?php if (count($categories)) { ?>
        <div class="row">
            <div class="col-12">
                <h3>
                    Subcategorii din <b><?php echo $this->fetch('title'); ?></b>
                </h3>
            </div>
            <?php foreach ($categories as $category) { ?>
                <div class="col-6 col-md-3 mb-5 d-flex align-items-stretch">
                    <div class="card">
                        <div class="card-image">
                            <?php
                            if (!empty($category->featured_image)) {
                                $image = $this->Thumb->fit('..' . $category->featured_image,  [
                                    'width' => 300,
                                    'height' => 200,
                                    'format' => 'jpg',
                                    'quality' => 75
                                ]);
                            } else {
                                $image = $this->Html->image('no-image.jpg', ['class' => 'card-img-top']);
                            }
                            echo $image;
                            ?>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">
                                <?php echo strip_tags($category->title); ?>
                            </h5>
                            <p class="card-text">
                                <?php echo strip_tags($category->description); ?>
                            </p>

                            <?php echo $this->Html->link('Vezi mai multe', ['controller' => 'Categories', 'action' => 'category', $parentCategory->slug, $category->slug], ['class' => 'btn btn-primary stretched-link']) ?>

                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class="row">
            <div class="col-12">
                <h3>
                    Nu avem subcategorii.
                </h3>
            </div>
        </div>
    <?php }  ?>
    <?php if (count($products)) { ?>
        <div class="row">
            <div class="col-12">
                <h3>
                    Produse din <b><?php echo $this->fetch('title'); ?></b>
                </h3>
            </div>
            <?php foreach ($products as $product) { ?>
                <div class="col-6 col-sm-3 mb-5 d-flex align-items-stretch">
                    <div class="card">
                        <div class="card-image">
                            <?php
                            if (!empty($product->featured_image)) {
                                $image = $this->Thumb->fit('..' . $product->featured_image,  [
                                    'width' => 300,
                                    'height' => 200,
                                    'format' => 'jpg',
                                    'quality' => 75
                                ]);
                            } else {
                                $image = $this->Html->image('no-image.jpg', ['class' => 'card-img-top']);
                            }
                            echo $image;
                            ?>
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">
                                <?php echo strip_tags($product->title); ?>
                            </h5>
                            <p class="card-text">
                                <?php echo strip_tags($product->description); ?>
                                <br>
                                <strong>Preț: <?php echo strip_tags($product->price); ?> LEI </strong>
                            </p>
                            <?php echo $this->Html->link(
                                'Vezi mai multe',
                                ['prefix' => false, 'controller' => 'Products', 'action' => 'view', $parentCategory->slug, $product->slug],
                                ['class' => 'btn btn-primary stretched-link']
                            ) ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } else { ?>
        <div class="row">
            <div class="col-12">
                <h3>
                    Nu avem produse.
                </h3>
            </div>
        </div>
    <?php }  ?>
</div>