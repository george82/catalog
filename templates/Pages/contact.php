<?php $this->assign('title', __('Contact')); ?>
<div class="jumbotron">
    <div class="jumbotron-image">
    </div>
    <div class="jumbotron-text">
        <div class="container">
            <div class="row">
                <div class="col-12 my-5">
                    <div class="mb-2">
                        <h2>
                            <?php echo $this->fetch('title'); ?>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Form->create($contact, ['role' => 'form']); ?>
<div class="container">
    <div class="row">
        <div class="col-12">
            <?php
            $this->Breadcrumbs->reset()->add($this->fetch('title'), NULL);

            $this->Breadcrumbs->prepend([
                ['title' => __('Acasă'), 'url' => ['controller' => 'Pages', 'action' => 'index']],
            ]);

            echo $this->Breadcrumbs->render(
                ['class' => 'breadcrumbs-trail'],
                ['separator' => '/']
            );
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-4">
            <div class="mb-4">
                <?php
                echo $this->Form->control('name', ['label' => 'Nume']);
                echo $this->Form->control('phone', ['label' => 'Telefon']);
                echo $this->Form->control('email', ['label' => 'Email']);
                ?>
            </div>

            <div class="mb-4">
                <?php
                echo $this->Form->control('message', [
                    'label' => 'Mesaj',
                    'style' => 'height: 160px;',

                ]);
                ?>
            </div>

            <div class="text-end">
                <?php echo $this->Form->submit(__('Trimite'), ['class' => 'ms-auto btn btn-primary']); ?>
            </div>

        </div>
        <div class="col-12 col-md-8">

            <?php echo $this->element('front/map') ?>

        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>