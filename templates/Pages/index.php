<?php $this->assign('title', __('Acasă')); ?>

<div class="container">
    <div class="row">
        <div class="col-12">
            <?php echo $this->element('front/slider'); ?>
        </div>
    </div>
    <?php echo $this->element('front/categories'); ?>
    <?php echo $this->element('front/products'); ?>

</div>