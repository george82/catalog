 <?php $this->assign('title', $product->title); ?>

 <?php
    // $css = [
    //     '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css'
    // ];

    // $script = [
    //     '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js'
    // ];

    $css = [
        '/vendors/fancybox-3.2.0/css/jquery.fancybox.min',
        '/vendors/OwlCarousel2-2.3.4/assets/owl.carousel.min',
        '/vendors/OwlCarousel2-2.3.4/assets/owl.theme.default.min',
    ];

    $script = [
        '/vendors/fancybox-3.2.0/js/jquery.fancybox.min',
        '/vendors/OwlCarousel2-2.3.4/owl.carousel.min',
    ];
    $this->Html->css($css, array('inline' => false, 'block' => 'css'));
    $this->Html->script($script, array('inline' => false, 'block' => 'scriptsForPage'));


    ?>

 <div class="container">
     <div class="row">
         <div class="col-12">
             <?php
                $this->Breadcrumbs->reset()->add($this->fetch('title'), NULL);


                if (isset($category) && !empty($category)) {
                    $this->Breadcrumbs->prepend([
                        ['title' => $category->title, 'url' => ['controller' => 'Categories', 'action' => 'category', $parentCategory->slug, $category->slug]],
                    ]);
                }
                $this->Breadcrumbs->prepend([
                    ['title' => $parentCategory->title, 'url' => ['controller' => 'Categories', 'action' => 'mainCategory', $parentCategory->slug]],
                ]);
                $this->Breadcrumbs->prepend([
                    ['title' => __('Categorii'), 'url' => ['controller' => 'Categories', 'action' => 'index']],
                ]);

                $this->Breadcrumbs->prepend([
                    ['title' => __('Acasă'), 'url' => ['controller' => 'Pages', 'action' => 'index']],
                ]);

                echo $this->Breadcrumbs->render(
                    ['class' => 'breadcrumbs-trail'],
                    ['separator' => '/']
                );

                ?>
         </div>
     </div>
     <div class="row mt-5">
         <div class="col-12 col-sm-6 col-md-6">
             <div class="image">
                 <?php
                    // if (!empty($product->featured_image)) {
                    //     $image = $this->Thumb->resize('..' . $product->featured_image,  [
                    //         'width' => 400,
                    //         'format' => 'jpg',
                    //         'quality' => '70'
                    //     ]);
                    // } else {
                    //     $image = $this->Html->image('no-image.jpg', ['class' => 'w-100']);
                    // }
                    // echo $image;
                    ?>


                 <?php
                    $img = $this->Custom->imageResize(
                        [
                            'model_name' => $product->model_name,
                            'uuid' => $product->uuid,
                            'featured_image' => $product->featured_image
                        ],
                        [
                            'width' => 1024,
                            'height' => 1024,
                            'format' => 'png'
                        ]
                    );
                    $imgUrl = $this->Custom->imageResizeUrl(
                        [
                            'model_name' => $product->model_name,
                            'uuid' => $product->uuid,
                            'featured_image' => $product->featured_image
                        ],
                        [
                            'width' => 1024,
                            'height' => 1024,
                            'format' => 'png'
                        ]
                    );

                    if (isset($product->gallery_images) && !empty($product->gallery_images)) {
                        $i = 0;
                        foreach ($product->gallery_images as $image) {
                            $i++;
                        }

                        if ($i == 1) { ?>
                         <div class="image">
                             <?php

                                if (isset($image['video_code']) && !empty($image['video_code'])) { ?>
                                 <a href="https://www.youtube.com/watch?v=<?php echo $image['video_code']; ?>" data-fancybox="gallery" data-caption="<?php echo $image['title'] ?>" class="play">
                                     <?php echo $img; ?>
                                 </a>
                             <?php } else { ?>
                                 <?php echo $this->Html->link($img, $imgUrl, ['escape' => false, 'data-fancybox' => 'gallery']); ?>
                             <?php } ?>
                         </div>
                     <?php } else { ?>
                         <?php echo $this->element('front/product-slider');
                            ?>
                     <?php } ?>
                 <?php } else if (isset($product->featured_image) && !empty($product->featured_image)) { ?>
                     <div class="image single">
                         <?php echo $this->Html->link($img, $imgUrl, ['escape' => false, 'data-fancybox' => 'gallery']); ?>
                     </div>
                 <?php }  ?>
                 <?php if (isset($product->is_repairable) && ($product->is_repairable == true)) { ?>
                     <?php echo $this->Html->link($this->Html->image('front/reparabilitate-10-ani.png'), ['controller' => 'Pages', 'action' => 'reparability'], ['escape' => false, 'class' => 'link-reparability']) ?>
                 <?php } ?>

             </div>

         </div>
         <div class="col-12 col-sm-6 col-md-6">
             <h2>
                 <?php echo $this->fetch('title'); ?>
             </h2>
             <h3>
                 Preț: <?php echo h($product->price) ?> LEI
             </h3>
             <div class="description">
                 <?php echo strip_tags($product->description) ?>
             </div>
         </div>
         <?php /* ?>
         <div class="row gallery">
             <div class="col-12">
                 <h3>
                     Galerie
                 </h3>
             </div>
             <?php foreach ($product->gallery_images as $image) { ?>
                 <div class="col-6 col-md-3 my-auto py-2">
                     <?php # echo $image['image_path']; 
                        $img = $this->Thumb->resize('..' . $image['image_path'],  [
                            'width' => 400,
                            'format' => 'jpg',
                            'quality' => '70',
                            'class' => 'w-100'
                        ]);
                        echo $img;
                        ?>
                     <?php if (isset($image['title']) && !empty($image['title'])) {
                            echo $image['title'];
                        } ?>
                     <?php if (isset($image['video_code']) && !empty($image['video_code'])) {
                            echo $image['video_code'];
                        } ?>
                 </div>
             <?php } ?>
         </div>
    <?php */ ?>

     </div>
 </div>
 <?php $this->start('scriptBottom'); ?>
 <script>
     $(document).ready(function() {

         var sync1 = $("#slider");
         var sync2 = $("#carousel");
         var slidesPerPage = 6; //globaly define number of elements per page
         var syncedSecondary = true;

         sync1.owlCarousel({
             items: 1,
             slideSpeed: 2000,
             autoplay: false,
             nav: true,
             dots: true,
             loop: true,
             responsiveRefreshRate: 200,
             navText: ['<span><</span>', '<span>></span>']
         }).on('changed.owl.carousel', syncPosition);

         sync2
             .on('initialized.owl.carousel', function() {
                 sync2.find(".owl-item").eq(0).addClass("current");
             })
             .owlCarousel({
                 items: slidesPerPage,
                 dots: true,
                 nav: true,
                 smartSpeed: 200,
                 slideSpeed: 500,
                 slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
                 responsiveRefreshRate: 100,
                 navText: ['<span><</span>', '<span>></span>']
             }).on('changed.owl.carousel', syncPosition2);

         function syncPosition(el) {
             //if you set loop to false, you have to restore this next line
             //var current = el.item.index;

             //if you disable loop you have to comment this block
             var count = el.item.count - 1;
             var current = Math.round(el.item.index - (el.item.count / 2) - .5);

             if (current < 0) {
                 current = count;
             }
             if (current > count) {
                 current = 0;
             }

             //end block

             sync2
                 .find(".owl-item")
                 .removeClass("current")
                 .eq(current)
                 .addClass("current");
             var onscreen = sync2.find('.owl-item.active').length - 1;
             var start = sync2.find('.owl-item.active').first().index();
             var end = sync2.find('.owl-item.active').last().index();

             if (current > end) {
                 sync2.data('owl.carousel').to(current, 100, true);
             }
             if (current < start) {
                 sync2.data('owl.carousel').to(current - onscreen, 100, true);
             }
         }

         function syncPosition2(el) {
             if (syncedSecondary) {
                 var number = el.item.index;
                 sync1.data('owl.carousel').to(number, 100, true);
             }
         }

         sync2.on("click", ".owl-item", function(e) {
             e.preventDefault();
             var number = $(this).index();
             sync1.data('owl.carousel').to(number, 300, true);
         });

         $('#slider .owl-item:not(.cloned) a[data-fancybox="gallery"]').fancybox({
             'data-fancybox': 'gallery'
         });
     });
 </script>
 <?php $this->end(); ?>