<?php $this->assign('title', __('Rezultatele căutarii')); ?>


<div class="jumbotron">
    <div class="jumbotron-image">
    </div>
    <div class="jumbotron-text">
        <div class="container">
            <div class="row">
                <div class="col-12 my-5">
                    <div class="mb-2">
                        <h2>
                            <?php echo $this->fetch('title'); ?>
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="container">
    <div class="row">
        <div class="col-12">
            <?php
            $this->Breadcrumbs->reset()->add(__('Rezultatele căutarii'), NULL);
            $this->Breadcrumbs->prepend([
                ['title' => __('Acasă'), 'url' => ['controller' => 'Pages', 'action' => 'index']],
            ]);
            echo $this->Breadcrumbs->render(
                ['class' => 'breadcrumbs-trail'],
                ['separator' => '>']
            );
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-4 my-3">
            <?php echo $this->element('front/search'); ?>
        </div>
        <div class="col-12 col-sm-12 col-md-12">
            <div class="">
                <?php if (!empty($q)) {
                    if (!empty($results['categories']) || !empty($results['products'])) { ?>
                        <section class="section-search-result">
                            <h4>
                                <?php echo __('Ați căutat: ' . $q);  ?>
                            </h4>
                        </section>
                        <?php if (!empty($results['categories'])) { ?>
                            <div class="row">
                                <div class="col-12 mb-3">
                                    <h2>
                                        Categorii
                                    </h2>
                                </div>
                            </div>
                            <div class="row">
                                <?php foreach ($results['categories'] as $category) { ?>
                                    <div class="col-6 col-md-3 mb-5 d-flex align-items-stretch">
                                        <div class="card">
                                            <div class="card-image">
                                                <?php
                                                if (!empty($category->featured_image)) {
                                                    $image = $this->Thumb->resize('..' . $category->featured_image,  ['width' => 200, 'height' => 200, 'format' => 'png']);
                                                } else {
                                                    $image = $this->Html->image('no-image.jpg', ['class' => 'card-img-top']);
                                                }
                                                echo $image;
                                                ?>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title">
                                                    <?php echo strip_tags($category->title); ?>
                                                </h5>
                                                <p class="card-text">
                                                    <?php echo strip_tags($category->description); ?>
                                                </p>

                                                <?php echo $this->Html->link(
                                                    'Vezi mai multe',
                                                    (!empty($category->parent_category)) ?
                                                        ['prefix' => false, 'controller' => 'Categories', 'action' => 'category', $category->parent_category->slug, $category->slug]
                                                        :
                                                        ['prefix' => false, 'controller' => 'Categories', 'action' => 'mainCategory',  $category->slug],
                                                    ['class' => 'btn btn-primary stretched-link']
                                                ) ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <?php if (!empty($results['products'])) { ?>
                            <div class="row">
                                <div class="col-12 mb-3">
                                    <h2>
                                        Produse
                                    </h2>
                                </div>
                            </div>
                            <div class="row">
                                <?php foreach ($results['products'] as $product) { ?>
                                    <div class="col-6 col-sm-3 mb-5 d-flex align-items-stretch">
                                        <div class="card">
                                            <div class="card-image">
                                                <?php
                                                if (!empty($product->featured_image)) {
                                                    $image = $this->Thumb->resize('..' . $product->featured_image,  ['width' => 200, 'height' => 200, 'format' => 'png']);
                                                } else {
                                                    $image = $this->Html->image('no-image.jpg', ['class' => 'card-img-top']);
                                                }
                                                echo $image;
                                                ?>
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-title">
                                                    <?php echo strip_tags($product->title); ?>
                                                </h5>
                                                <p class="card-text">
                                                    <?php echo strip_tags($product->description); ?>
                                                    <br>
                                                    <strong>Preț: <?php echo strip_tags($product->price); ?> LEI </strong>
                                                </p>
                                                <?php echo $this->Html->link(
                                                    'Vezi mai multe',
                                                    ['prefix' => false, 'controller' => 'Products', 'action' => 'view', !empty($product->category->parent_category->slug) ? $product->category->parent_category->slug : '', $product->category->slug, $product->slug],
                                                    ['class' => 'btn btn-primary stretched-link']
                                                ) ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>


                        <?php } else { ?>
                            <section class="section-search-result">
                                <h2>
                                    <?php echo __('Nu sunt rezultate'); ?>
                                </h2>
                            </section>
                        <?php }
                } else { ?>
                        <div class="text-center">
                            <h3>
                                <?php echo __('Nu sunt rezultate'); ?>
                            </h3>
                        </div>
                    <?php } ?>



                            </div>
            </div>
        </div>

    </div>





    <?php $this->start('scriptBottom'); ?>
    <script>
        $(".product .title").matchHeight();
        $(".product .description").matchHeight();
    </script>
    <?php $this->end(); ?>