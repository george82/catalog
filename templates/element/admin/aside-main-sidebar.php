<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <?php # echo $this->element('admin/aside/user-panel'); 
        ?>

        <!-- search form -->
        <?php # echo $this->element('admin/aside/form'); 
        ?>
        <!-- /.search form -->

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <?php echo $this->element('admin/aside/sidebar-menu'); ?>

    </section>
    <!-- /.sidebar -->
</aside>