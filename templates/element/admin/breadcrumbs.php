<?php $this->Breadcrumbs->add('<i class="fa fa-dashboard"></i> ' . __('Home', '#'));

if(!empty($breadcrumbs)) {
    $this->Breadcrumbs->add(@$breadcrumbs);
}
$parameters = $this->request->getAttribute('params');

$this->Breadcrumbs->add($this->request->getParam('controller'), ['controller' => $this->request->getParam('controller'), 'action' => 'index']);
$this->Breadcrumbs->add($this->request->getParam('action', '#'));

$this->Breadcrumbs->setTemplates([
    'wrapper' => '<ol{{attrs}}>{{content}}</ol>',
    'separator' => '<!-- separator -->'
]);

echo $this->Breadcrumbs->render(
    ['class' => 'breadcrumb'],
    ['separator' => null]
);
