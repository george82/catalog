<div class="<?= $class ?>">
    <div class="form-group input text">
        <label class="control-label" for="term"><? __('Name') ?></label>
        <div class="input-group">
            <input type="text" name="term" id="term" class="form-control pull-right" placeholder="<?= __('Search'); ?>" value="<?= $filter->term ?>">
            <div class="input-group-btn">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
            </div>
        </div>
    </div>
</div>