<footer class="main-footer">
  <?php if (isset($layout) && $layout == 'top') : ?>
    <div class="container">
    <?php endif; ?>
    <div class="pull-right hidden-xs">
      <!-- <b>Version</b> 0.0.1 -->
    </div>
    <strong>Copyright &copy; <?= date('Y'); ?> Gheorghe Stănescu
      <?php if (isset($layout) && $layout == 'top') : ?>
    </div>
  <?php endif; ?>
</footer>