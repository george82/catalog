<div class="row">
    <div class="col-12 mb-3">
        <h2>
            Categorii
        </h2>
    </div>
</div>
<div class="row">
    <?php foreach ($categories as $category) { ?>
        <div class="col-6 col-md-3 mb-5 d-flex align-items-stretch">
            <div class="card">
                <div class="card-image">
                    <?php
                    if (!empty($category->featured_image)) {
                        $image = $this->Thumb->fit('..' . $category->featured_image,  [
                            'width' => 300,
                            'height' => 200,
                            'format' => 'jpg',
                            'quality' => 75
                        ]);
                    } else {
                        $image = $this->Html->image('no-image.jpg', ['class' => 'card-img-top']);
                    }
                    echo $image;
                    ?>
                </div>
                <div class="card-body">
                    <h5 class="card-title">
                        <?php echo strip_tags($category->title); ?>
                    </h5>
                    <p class="card-text">
                        <?php echo strip_tags($category->description); ?>
                    </p>

                    <?php echo $this->Html->link(
                        'Vezi mai multe',
                        (!empty($category->parent_category)) ?
                            ['prefix' => false, 'controller' => 'Categories', 'action' => 'category', $category->parent_category->slug, $category->slug]
                            :
                            ['prefix' => false, 'controller' => 'Categories', 'action' => 'mainCategory',  $category->slug],
                        ['class' => 'btn btn-primary stretched-link']
                    ) ?>

                </div>
            </div>
        </div>
    <?php } ?>
</div>