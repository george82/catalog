<footer class="bg-light">
    <div class="container">
        <div class="row">
            <div class="col-12">
                &copy; <span class="logo-lg"><b>Cata</b>LoG</span> <?php echo date('Y'); ?>
            </div>
        </div>
    </div>
</footer>