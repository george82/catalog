<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <?php echo $this->Html->link('<span class="logo-lg"><b>Cata</b>LoG</span>', ['controller' => 'Pages', 'action' => 'index'], ['class' => ($this->request->getParam('action') == 'index') ? 'navbar-brand active' : 'navbar-brand', 'escape' => false]) ?>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <?php echo $this->Html->link('Acasă', ['controller' => 'Pages', 'action' => 'index'], ['class' => (($this->request->getParam('controller') == 'Pages') && ($this->request->getParam('action') == 'index')) ? 'nav-link active' : 'nav-link']) ?>
                </li>
                <li class="nav-item">
                    <?php echo $this->Html->link('Categorii', ['controller' => 'Categories', 'action' => 'index'], ['class' => (($this->request->getParam('controller') == 'Categories')) ? 'nav-link active' : 'nav-link']) ?>
                </li>
                <li class="nav-item">
                    <?php echo $this->Html->link('Contact', ['controller' => 'Pages', 'action' => 'contact'], ['class' => (($this->request->getParam('controller') == 'Pages') && ($this->request->getParam('action') == 'contact')) ? 'nav-link active' : 'nav-link']) ?>
                </li>
                <?php /* ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Dropdown
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li><a class="dropdown-item" href="#">Action</a></li>
                        <li><a class="dropdown-item" href="#">Another action</a></li>
                        <li>
                            <hr class="dropdown-divider">
                        </li>
                        <li><a class="dropdown-item" href="#">Something else here</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                </li>
                <?php */ ?>
            </ul>
            <div class="d-flex ms-lg-5">
                <?php echo $this->element('front/search'); ?>
            </div>
        </div>
    </div>
</nav>