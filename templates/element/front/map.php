<style>
    .map {
        height: 100%;
        min-height: 260px;
    }
</style>
<div id="map" class="map"></div>
<script>
    const LAT = 44.440549682805;
    const LNG = 26.04462919041826;

    function initMap() {
        var myLatLng = {
            lat: LAT,
            lng: LNG
        };
        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            scrollwheel: false,
            zoom: 15
        });

        // Create a marker and set its position.
        const marker = new google.maps.Marker({
            map: map,
            position: myLatLng,
            icon: '/img/map-marker.png'
        });



        const contentString =
            '<div id="content">' +
            '<div id="siteNotice">' +
            "</div>" +
            '<div id="bodyContent">' +
            '<p><br/><a href="https://maps.google.com/?q=' + LAT + ',' + LNG + '" target="_blank" class="btn btn-primary btn-sm">Deschide în Google Maps</a>' + '</p>' +
            "</div>" +
            "</div>";

        const infowindow = new google.maps.InfoWindow({
            content: contentString,
        });

        // const marker = new google.maps.Marker({
        //     position: uluru,
        //     map,
        //     title: "Uluru (Ayers Rock)",
        // });
        marker.addListener("click", () => {
            infowindow.open(map, marker);
        });

    }
</script>
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyDV78HA8tf7FTyosWxx7L3H1gODpgujCTY&callback=initMap" async defer></script>