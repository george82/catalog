<div class="product-slider">
    <div class="slider-wrapper">
        <div id="slider" class="owl-carousel">
            <?php foreach ($product->gallery_images as $image) { ?>
                <div class="image">
                    <?php

                    $img = $this->Custom->imageResize(
                        [
                            'model_name' => $image->model_name,
                            'uuid' => $image->uuid,
                            'featured_image' => $image->image_path
                        ],
                        [
                            'width' => 1024,
                            'height' => 1024,
                            'format' => 'png'
                        ]
                    );
                    $imgUrl = $this->Custom->imageResizeUrl(
                        [
                            'model_name' => $image->model_name,
                            'uuid' => $image->uuid,
                            'featured_image' => $image->image_path
                        ],
                        [
                            'width' => 1024,
                            'height' => 1024,
                            'format' => 'png'
                        ]
                    );
                    ?>
                    <?php if (isset($image['video_code']) && !empty($image['video_code'])) { ?>
                        <a href="https://www.youtube.com/watch?v=<?php echo $image['video_code']; ?>" data-fancybox="gallery" data-caption="<?php echo $image['title'] ?>" class="play">
                            <?php echo $img; ?>
                        </a>
                    <?php } else { ?>
                        <?php echo $this->Html->link($img, $imgUrl, ['escape' => false, 'data-fancybox' => 'gallery']); ?>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="carousel-wrapper">
        <div id="carousel" class="owl-carousel">
            <?php foreach ($product->gallery_images as $image) { ?>
                <div class="thumbnail">
                    <?php

                    $image = $this->Custom->imageResize(
                        [
                            'model_name' => $image->model_name,
                            'uuid' => $image->uuid,
                            'featured_image' => $image->image_path
                        ],
                        [
                            'width' => 85,
                            'height' => 85,
                            'format' => 'png'
                        ]
                    );
                    echo $image;
                    if (isset($image['video_code']) && !empty($image['video_code'])) { ?>
                        play
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>