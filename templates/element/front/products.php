<div class="row">
    <div class="col-12 mb-3">
        <h2>
            Produse
        </h2>
    </div>
</div>
<div class="row">
    <?php foreach ($products as $product) { ?>
        <div class="col-6 col-sm-3 mb-5 d-flex align-items-stretch">
            <div class="card">
                <div class="card-image">
                    <?php
                    if (!empty($product->featured_image)) {
                        $image = $this->Thumb->fit('..' . $product->featured_image,  [
                            'width' => 300,
                            'height' => 200,
                            'format' => 'jpg',
                            'quality' => 75
                        ]);
                    } else {
                        $image = $this->Html->image('no-image.jpg', ['class' => 'card-img-top']);
                    }
                    echo $image;
                    ?>
                </div>
                <div class="card-body">
                    <h5 class="card-title">
                        <?php echo strip_tags($product->title); ?>
                    </h5>
                    <p class="card-text">
                        <?php echo strip_tags($product->description); ?>
                        <br>
                        <strong>Preț: <?php echo strip_tags($product->price); ?> LEI </strong>
                    </p>
                    <?php echo $this->Html->link(
                        'Vezi mai multe',
                        ['prefix' => false, 'controller' => 'Products', 'action' => 'view', !empty($product->category->parent_category->slug) ? $product->category->parent_category->slug : '', $product->category->slug, $product->slug],
                        ['class' => 'btn btn-primary stretched-link']
                    ) ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>