<div class="searchForm">
    <?php
    echo $this->Form->create(null, ['url' => ['prefix' => false, 'controller' => 'Searches', 'action' => 'index'], 'type' => 'get', 'class' => 'd-flex',]);
    echo $this->Form->text(
        'q',
        [
            'label' => false,
            'value' => isset($q) ? $q : '',
            'placeholder' => __('Caută'),
            'class' => 'form-control me-2',
        ]
    );

    echo $this->Form->submit(
        'Caută',
        [
            'type' => 'submit',
            'class' => 'btn btn-outline-primary',
            'escape' => false
        ]
    );
    echo $this->Form->end();
    ?>
</div>