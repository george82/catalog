<div id="carouselExampleCaptions" class="carousel carousel-dark slide" data-bs-ride="carousel">
    <div class="carousel-indicators">

        <?php
        $i = 0;
        foreach ($categories as $key => $category) {
        ?>
            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="<?php echo $i; ?>" class="active" aria-current="true" aria-label="Slide 1"></button>

        <?php
            $i++;
        } ?>

    </div>
    <div class="carousel-inner">
        <?php
        $i = 0;
        foreach ($categories as $key => $category) {
        ?>
            <div class="carousel-item <?php echo ($i == 0) ? 'active' : 'inactive'; ?>">
                <div class="image">
                    <?php
                    if (!empty($category->featured_image)) {
                        $image = $this->Thumb->fit('..' . $category->featured_image,  [
                            'width' => 1200,
                            'height' => 500,
                            'format' => 'jpg',
                            'quality' => '85'
                        ]);
                    } else {
                        $image = $this->Html->image('no-image.jpg', ['class' => 'card-img-top']);
                    }
                    echo $image;
                    ?>
                </div>

                <div class="carousel-caption d-none d-md-block">
                    <h5><?php echo strip_tags($category->title); ?></h5>
                    <p><?php echo strip_tags($category->description); ?></p>
                    <div class="actions">
                        <?php echo $this->Html->link('Vezi mai multe', ['controller' => 'Categories', 'action' => 'category',  $category->slug], ['class' => 'btn btn-primary']) ?>
                    </div>

                </div>
            </div>
        <?php
            $i++;
        } ?>
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>