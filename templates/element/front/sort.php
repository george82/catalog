<div class="sortForm">
    <?php
    echo $this->Form->create(null, ['type' => 'get', 'class' => 'd-flex justify-content-end']);
    ?>
    <label class="my-auto pe-3">
        Sortează după:
    </label>
    <?php

    echo $this->Form->select(
        'sortare',
        [
            'nume-alfabetic' => 'Nume A-Z',
            'nume-invers-alfabetic' => 'Nume Z-A',
            'pret-crescator' => 'Preț ↑',
            'pret-descrescator' => 'Preț ↓'
        ],
        [
            'default' => (isset($sort) ? $sort : 'nume-alfabetic'),
            'class' => 'form-select',
            'style' => 'max-width: 40%'
        ]
    );

    // echo $this->Form->button(
    //     'Sortează',
    //     [
    //         'type' => 'submit',
    //         'class' => 'btn btn-outline-primary ms-3',
    //         'escape' => false
    //     ]
    // );
    echo $this->Form->end();

    ?>


</div>

<?php $this->start('scriptBottom'); ?>
<script>
    $(document).on('change', '.sortForm select', function() {
        $('.sortForm form').submit();
    });
</script>
<?php $this->end(); ?>