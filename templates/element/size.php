<?php

//echo $_SERVER['REMOTE_ADDR'];
if (in_array($_SERVER['REMOTE_ADDR'], array("5.2.216.144", "86.121.195.42", "::1", "127.0.0.1"))) { ?>
    <div id="windowSize">
        <table>
            <tr>
                <td>
                    <b>
                        <span class="width"></span>
                    </b>
                </td>
                <td>
                    ×
                </td>
                <td>
                    <b>
                        <span class="height"></span>
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    layout
                </td>
                <td>
                    :
                </td>
                <td>
                    <?= strtolower($this->layout); ?>
                </td>
            </tr>
            <tr>
                <td>
                    controller
                </td>
                <td>
                    :
                </td>
                <td>
                    <?= strtolower($this->request->getParam('controller')); ?>
                </td>
            </tr>
            <tr>
                <td>
                    action
                </td>
                <td>
                    :
                </td>
                <td>
                    <?= strtolower($this->request->getParam('action')); ?>
                </td>
            </tr>
        </table>
        <p style="text-align: center;">
            <?php echo $_SERVER['REMOTE_ADDR']; ?>
        </p>
    </div>
    <style>
        #windowSize {
            position: fixed;
            right: 0px;
            bottom: 40px;
            color: #f00;
            z-index: 100;
            background-color: rgba(0, 0, 0, 0.25);
            padding: 3px;
            text-align: right;
            font-weight: normal;
            font-family: Arial, sans-serif;
        }

        #windowSize td:nth-child(2n) {
            text-align: center;
            min-width: 10px;
        }

        #windowSize td:nth-child(3n) {
            text-align: left;
        }

        #windowSize td,
        #windowSize p {
            font-size: 12px;
            color: red;
            margin: 0;
        }

        #windowSize p span {
            display: inline-block;
            padding: 0 3px;
            font-weight: bold;
        }
    </style>
    <?php $this->append('jsForTemplate') ?>
    <?php $this->end() ?>
    <script>
        $(window).on('load resize', function() {
            $('#windowSize .width').html($(window).width());
            $('#windowSize .height').html($(window).height());
        });
    </script>

<?php } ?>