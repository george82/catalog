<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>

<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $this->fetch('title') ?> | CataLoG
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css([
        '//cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css',
        // '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css',
        'main'
    ]) ?>


    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
</head>

<body>
    <?php echo $this->element('front/header') ?>
    <main class="main">
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </main>
    <?php echo $this->element('front/footer') ?>

    <?php $script = [
        // '//code.jquery.com/jquery-3.6.0.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js',
        // '//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js',
        '//cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js',
        'jquery.matchHeight',
        'main'
    ] ?>

    <?php # echo $this->fetch('script'); 
    ?>

    <?php # echo $this->fetch('scriptForPage'); 
    ?>
    <?php # echo $this->fetch('scriptBottom'); 
    ?>

    <?php echo $this->Html->script($script) . PHP_EOL . $this->fetch('scriptBottom') . PHP_EOL . $this->fetch('scriptsForPage') . PHP_EOL; ?>

    <?php echo $this->element('size') ?>
</body>

</html>