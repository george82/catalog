<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GalleryImagesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GalleryImagesTable Test Case
 */
class GalleryImagesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\GalleryImagesTable
     */
    protected $GalleryImages;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.GalleryImages',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('GalleryImages') ? [] : ['className' => GalleryImagesTable::class];
        $this->GalleryImages = $this->getTableLocator()->get('GalleryImages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->GalleryImages);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
