/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

    'use strict';


    // bootstrap WYSIHTML5 - text editor
    // $('textarea').wysihtml5();

    $('textarea').wysihtml5({
        toolbar: {
            "font-styles": true, // Font styling, e.g. h1, h2, etc.
            "emphasis": true, // Italics, bold, etc.
            "lists": true, // (Un)ordered lists, e.g. Bullets, Numbers.
            "html": true, // Button which allows you to edit the generated HTML.
            "link": true, // Button to insert a link.
            "image": true, // Button to insert an image.
            "color": false, // Button to change color of font
            "blockquote": true, // Blockquote
            "size": 'xs' // options are xs, sm, lg
        }
    });


});
Dropzone.options.galleryDropzone = {
    maxFilesize: 10, // Mb
    init: function () {
        // Set up any event handlers
        // this.on('completemultiple', function () {
        //     location.reload();
        // });
        this.on('queuecomplete', function () {
            location.reload();
        });
    }
};
